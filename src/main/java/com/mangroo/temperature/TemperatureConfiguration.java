package com.mangroo.temperature;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.InstanceProfileCredentialsProvider;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.mangroo.temperature.business.TemperatureService;
import com.mangroo.temperature.data.DynamoDBConnectionType;
import com.mangroo.temperature.data.repo.TemperatureRepository;

@SpringBootConfiguration
@EnableAutoConfiguration
@ComponentScan
@EnableDynamoDBRepositories(basePackages = "com.mangroo.temperature.data.repo")
public class TemperatureConfiguration {

	@Value("${amazon.aws.accesskey}")
	private String amazonAWSAccessKey;

	@Value("${amazon.aws.secretkey}")
	private String amazonAWSSecretKey;

	@Value("${amazon.dynamodb.endpoint:}")
	private String amazonDynamoDBEndpoint;

	@Value("${amazon.dynamodb-connection-type}")
	private DynamoDBConnectionType dynamoDBConnectionType;

	Logger logger = LoggerFactory.getLogger(TemperatureConfiguration.class);

	@Bean
	public AWSCredentials amazonAWSCredentials() {
		return new BasicAWSCredentials(amazonAWSAccessKey, amazonAWSSecretKey);
	}

	public AWSCredentialsProvider amazonAWSCredentialsProvider() {
		return new AWSStaticCredentialsProvider(amazonAWSCredentials());
	}

	@Bean
	public AmazonDynamoDB amazonDynamoDB() {

		logger.info("DynamoDB Connection Type: {}", dynamoDBConnectionType);
		logger.info("DynamoDB Endpoint: {}", amazonDynamoDBEndpoint);
		AmazonDynamoDB amazonDynamoDB;

		switch (dynamoDBConnectionType) {
		case LOCAL: {
			amazonDynamoDB = AmazonDynamoDBClientBuilder.standard().withCredentials(amazonAWSCredentialsProvider())
					.withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(amazonDynamoDBEndpoint, "EU_WEST_3")).build();
			break;
		}
		case REMOTE_KEYS: {
			amazonDynamoDB = AmazonDynamoDBClientBuilder.standard().withCredentials(amazonAWSCredentialsProvider()).withRegion(Regions.EU_WEST_3).build();
			break;
		}
		case REMOTE_INSTANCE_PROFILE: {
			amazonDynamoDB = AmazonDynamoDBClientBuilder.standard().withCredentials(new InstanceProfileCredentialsProvider(false)).withRegion(Regions.EU_WEST_3).build();
			break;
		}
		default: {
			amazonDynamoDB = AmazonDynamoDBClientBuilder.defaultClient();
			break;
		}
		}

		return amazonDynamoDB;
	}

	@Bean
	public DynamoDBMapper dynamoDBMapper(final AmazonDynamoDB amazonDynamoDB, final DynamoDBMapperConfig config) {
		return new DynamoDBMapper(amazonDynamoDB, config);
	}

	@Bean
	public DynamoDBMapperConfig dynamoDBMapperConfig() {
		return DynamoDBMapperConfig.DEFAULT;
	}

	@Bean
	public TemperatureService temperatureService(final TemperatureRepository temperatureRepository) {
		return new TemperatureService(temperatureRepository);
	}

}
